
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
			int _valeur;
			Noeud * _ptrNoeudSuivant;
        };

    public:
        class iterator {
			protected:
				Noeud * _ptrNoeudCourant;
			
            public:
				iterator(Noeud * _ptrNoeudCourant){
					for (int x : V)
					
				}
           
                const iterator & operator++() {
                    return *this;
                }

                int& operator*() const {
                    static int nimpe;
                    return nimpe;
                }

                bool operator!=(const iterator &) const {
                    return false;
                }

                friend Liste; 
        };
        
    protected:
		Noeud * _ptrTete;

    public:
		Liste(){
			_ptrTete = nullptr;
		}
		
		~Liste(){
			clear();
		}
    
        void push_front(int val) {
			Noeud * noeud = new Noeud;
			noeud -> _valeur = valeur;
			noeud -> _ptrNoeudSuivant = _ptrTete;
			_ptrTete = noeud;
        }

        int& front() const {
            static int nimpe;
            return nimpe;
        }

        void clear() {
			Noeud* noeud;
			while (_ptrTete != nullptr){
				noeud = _ptrTete -> _ptrNoeudSuivant;
				delete _ptrTete;
				noeud = _ptrTete;
			}
        }

        bool empty() const {
			if (_ptrTete != nullptr){
				return false;
			}else{
				return true;
			}
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
			
            return iterator(_ptrTete);
        }
};

std::ostream& operator<<(std::ostream& os, const Liste&) {
    return os;
}

#endif

