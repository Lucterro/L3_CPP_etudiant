#include "Image.hpp"
#include <iostream>
#include <string>

Image::Image(int largeur, int hauteur) :
 _largeur(largeur), _hauteur(hauteur)
 {
	 _pixels = new int [largeur, hauteur];
 }
	 

int Image::getLargeur() const { return _largeur; }
int Image::getHauteur() const { return _hauteur; }	 

/*Version classique*/

/*int Image::getPixel(int i, int j) const { return _pixels[i+j*_largeur]; }
void Image::setPixel(int i, int j, int couleur) {
	if(couleur > 255){
		couleur = 255;
		_pixels [i+(j*_largeur)] = couleur;
	}
	else if (couleur < 0){
		couleur = 0;
		_pixels [i+(j*_largeur)] = couleur;
	}
	else {
		_pixels [i+(j*_largeur)] = couleur;
		}
}*/

/*Version référence*/

int Image::getPixel(int i, int j) const { return _pixels[i+j*_largeur]; }
int & Image::setPixel(int i, int j) { return _pixels[i+j*_largeur]; }

const int & Image::pixel(int i, int j) const{
	if(i <= largeur && j <= hauteur && i >= 0 && j >= 0){
		return _pixels [i+(j*_largeur)];
	}
	return _pixels [i+(j*_largeur)];
}

int & Image::pixel(int i, int j){
	if(i <= largeur && j <= hauteur && i >= 0 && j >= 0){
		return _pixels [i+(j*_largeur)];
	}
	return _pixels [i+(j*_largeur)];
}

void ecrirePnm(const Image & img, const std::string & nomFichier){
	ofs << "P2"+std::to_string (_largeur)+" "+ std::to_string(_hauteur) << end::ofs;
	
}

void remplir(Image & img){
	
}

Image::~Image(){
	delete [] _pixels;
}
