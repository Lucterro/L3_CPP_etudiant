#include <CppUTest/CommandLineTestRunner.h>
//#include <CppUTest/MemoryLeakDetectorMallocMacros.h>

int main(int argc, char ** argv) {
	MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
    return CommandLineTestRunner::RunAllTests(argc, argv);
}

