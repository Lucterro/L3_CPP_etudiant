#include <iostream>
#include <string>
#include <vector>

#include "Point.hpp"
#include "Couleur.hpp"
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

int main() {
	
	Point p0;
	p0._x = 0;
	p0._y = 0;
	
	Point p1;
	p1._x = 100;
	p1._y = 200;
	
	Couleur c;
	c._r = 1;
	c._g = 0;
	c._b = 0;
	
	Ligne l (c, p0, p1);
	l.afficher();
	
	PolygoneRegulier poly(c, p1, 50, 5);
	poly.afficher();

    return 0;
}

