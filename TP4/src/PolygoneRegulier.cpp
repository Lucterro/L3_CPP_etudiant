#include <iostream>
#include <string>
#include <math.h>
#include "FigureGeometrique.hpp"
#include "PolygoneRegulier.hpp"

PolygoneRegulier::PolygoneRegulier (const Couleur & couleur, const Point & centre, int rayon, int nbCotes) : 
	FigureGeometrique(couleur)
{	
	_nbPoints = nbCotes;
	_points = new Point[nbCotes];
	
	for(int i = 0; i < _nbPoints; i++){
		_points[i]._x = rayon*cos(i*(M_PI/nbCotes))+centre._x;
		_points[i]._y = rayon*sin(i*(M_PI/nbCotes))+centre._y;
	}	
}

void PolygoneRegulier::afficher() const {
	std::cout << "PolygoneRegulier " << getCouleur()._r << "_" << getCouleur()._g << "_" <<
	getCouleur()._b << " ";
	for(int i = 0; i < _nbPoints; i++){
		std::cout << _points[i]._x << "_" << _points[i]._y << " ";
	}
	std::cout << std::endl;
}

int PolygoneRegulier::getNbPoints() const { return _nbPoints; }

const Point & PolygoneRegulier::getPoint(int indice) const { 
	for(int i = 0; i < _nbPoints; i++){
		if(i == indice){
			return _points[i];
		}
	std::cout << "Pas de point a cet indice" << std::endl;
	}
}

PolygoneRegulier::~PolygoneRegulier(){
	delete[] _points;
}
