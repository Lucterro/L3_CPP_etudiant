#ifndef BIBLIOTHEQUE_HPP_
#define BIBLIOTHEQUE_HPP_

#include <iostream>
#include <string>

class Bibliotheque {
	public:
		const void afficher();
		void trierParAuteurEtTitre();
		void trierParAnnee();
		void lireFichier(const std::string & nomFichier);
		const void ecrireFichier(const std::string & nomFichier);
};
#endif
