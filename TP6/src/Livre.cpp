#include "Livre.hpp"
#include <iostream>
#include <string>

Livre::Livre() : 
	_titre ("Would you kindly"), 
	_auteur ("Andrew Ryan"),
	_annee (2007)
{}

Livre::Livre(const std::string & titre, const std::string & auteur, int annee) :
 _titre(titre), _auteur(auteur), _annee(annee) {
	 
	std::string str(";");
	std::size_t found = titre.find(str);
	if(found != std::string::npos){
		throw std::string ("erreur : titre non valide (';' non autorisé)");
	}
	
	found = auteur.find(str);
	if(found != std::string::npos){
		throw std::string ("erreur : auteur non valide (';' non autorisé)");
	}
	
	std::string str2("\n");
	found = auteur.find(str2);
	if(found != std::string::npos){
		throw std::string ("erreur : auteur non valide ('\n' non autorisé)");
	}
	
	found = titre.find(str2);
	if(found != std::string::npos){
		throw std::string ("erreur : titre non valide ('\n' non autorisé)");
	}
}

const std::string & Livre::getTitre() const {
	return _titre;
}

const std::string & Livre::getAuteur() const {
	return _auteur;
}

int Livre::getAnnee() const {
	return _annee;
}

bool Livre::operator < (const Livre & l) const {
	return (_auteur == l._auteur && _titre<l._titre || _auteur<l._auteur);
	return false;
}

bool Livre::operator == (const Livre & l) const {
	return (_auteur == l._auteur && _titre == l._titre && _annee == l._annee);
	return false;
}

std::ostream & operator <<(std::ostream os, Livre & l){
	os << l.getTitre() + ";" + l.getAuteur() + ";" + std::to_string(l.getAnnee());
	return os;
}
