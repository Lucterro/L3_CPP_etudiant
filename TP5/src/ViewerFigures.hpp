#ifndef VIEWERFIGURES_HPP_
#define VIEWERFIGURES_HPP_
#include <gtkmm.h>
#include "FigureGeometrique.hpp"
#include "ZoneDessin.hpp"

class ViewerFigures {
	private:
		Gtk::Main _kit;
		Gtk::Window _window;
		ZoneDessin Ds;
		
	public:
		ViewerFigures(int argc, char** argv): _kit(argc,argv){};
		void run();
	
};
#endif
