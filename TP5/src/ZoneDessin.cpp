#include "ZoneDessin.hpp"
#include <gtkmm.h>
#include <vector>
#include <iostream>

ZoneDessin::ZoneDessin(){
	Point p0;
	p0._x = 0;
	p0._y = 0;
	
	Point p1;
	p1._x = 100;
	p1._y = 200;
	
	Couleur c;
	c._r = 1;
	c._g = 0;
	c._b = 0;
	
	Ligne l (c, p0, p1);
	PolygoneRegulier poly(c, p1, 50, 5);

	_figures.push_back(new Ligne(c, p0, p1));
	_figures.push_back(new PolygoneRegulier (c, p1, 50, 5));
}

ZoneDessin::~ZoneDessin(){
}

bool ZoneDessin::on_expose_event(GdkEventExpose* event){
	Cairo::RefPtr<Cairo::Context> myContext = get_window()->create_cairo_context();
	for(FigureGeometrique* Geoptr : _figures){
		Geoptr->afficher(myContext);
	} 
	return false;
}

bool ZoneDessin::gererClic(GdkEventButton* event){
	return false;
}
