#ifndef ZONEDESSIN_HPP_
#define ZONEDESSIN_HPP_

#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

#include <vector>
#include <gtkmm.h>

class ZoneDessin : public Gtk::DrawingArea {
	private:
		std::vector<FigureGeometrique*> _figures;
	protected:
		bool on_expose_event(GdkEventExpose* event);
		bool gererClic(GdkEventButton* event);
	public:
		ZoneDessin();
		~ZoneDessin();
};
#endif
