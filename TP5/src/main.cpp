#include <iostream>
#include <string>
#include <vector>

#include "Point.hpp"
#include "Couleur.hpp"
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

int main(int argc, char ** argv) {
	
	ViewerFigures Vf (argc, argv);
	Vf.run();
    return 0;
}

