#include <cctype>
#include <iostream>
#include <iostream>
#include "fibonacci.h"
#include "Vecteur3.h"
using namespace std;

int main()
{
	cout << "Recursif : " << fibonacciRecursif(7) << endl;
	cout << "Iteratif : " << fibonacciIteratif(7) << endl;
	
    Vecteur3 v1 {13, 37, 56};
    Vecteur3 v2 {5, 10, 80};

    cout << "Premier affichage : ";
    v1.afficher();
    cout << "Deuxieme affichage : ";
    afficher(v1);

    cout << "Norme : "<< Calculnorme(v1) << endl;
    cout << "Produit scalaire : " << produitScalaire(v1, v2) << endl;
    cout << "Addition : "<< addition(v1, v2) << endl;

    return 0;
}
