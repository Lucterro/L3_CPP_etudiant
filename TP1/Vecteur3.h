#ifndef VECTEUR3_H
#define VECTEUR3_H

struct Vecteur3 {
    float x, y, z;
    void afficher();
};

void afficher(const Vecteur3 & v);

float Calculnorme(const Vecteur3 & v);

float produitScalaire(const Vecteur3 &v1, const Vecteur3 &v2);

float addition (const Vecteur3 &v1, const Vecteur3 &v2);

#endif //VECTEUR_H
