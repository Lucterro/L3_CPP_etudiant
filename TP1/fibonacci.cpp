#include "fibonacci.h"
using namespace std;

int fibonacciRecursif(int n) {
	if (n < 2)
		return n;
	else
		return fibonacciRecursif(n-1) + fibonacciRecursif(n-2);
}

int fibonacciIteratif(int n) {
    if (n == 0){
        return 0;
    }
	int u = 0;
	int v = 1;
	int i, t;

	for(i = 2; i <= n; i++) {
		t = u + v;
		u = v;
		v = t;
	}
	return v;
}
