#ifndef FIBONACCI_H
#define FIBONACCI_H

int fibonacciRecursif(int n);
int fibonacciIteratif(int n);

#endif
