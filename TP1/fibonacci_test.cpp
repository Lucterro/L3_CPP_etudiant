#include "fibonacci.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };
TEST(GroupFibonacci, test_fibonacci_1) { // premier test unitaire
    int listresult [] = {0,1,1,2,3};
    for (int i=0;i<5;i++){
        CHECK_EQUAL(listresult[i], fibonacciRecursif(i));
    }
}

TEST(GroupFibonacci, test_fibonacci_2) { // deuxieme test unitaire
    int listresult [] = {0,1,1,2,3};
    for (int i=0;i<5;i++){
        CHECK_EQUAL(listresult[i], fibonacciIteratif(i));
    }
}
