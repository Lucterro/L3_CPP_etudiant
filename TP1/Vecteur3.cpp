#include "Vecteur3.h"
#include <iostream>
#include <math.h>
using namespace std;

void Vecteur3::afficher() {
    cout << "(" << x << ", " << y << ", " << z << ")" << endl;
}

void afficher(const Vecteur3 &v) {
    cout << "(" << v.x << ", " << v.y << ", " << v.z << ")" << endl;
}

float Calculnorme(const Vecteur3 &v){
    double norme = sqrt(pow(v.x,2)+pow(v.y,2)+pow(v.z,2));
    return norme;
}

float produitScalaire(const Vecteur3 &v1, const Vecteur3 &v2){
    float ProdScal= v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
    return ProdScal;
}

float addition(const Vecteur3 &v1, const Vecteur3 &v2){
   float Somme = v1.x+v2.x + v1.y+v2.y + v1.z+v2.z;
   return Somme;
}
