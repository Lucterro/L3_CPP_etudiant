#include "Vecteur3.h"

#include <math.h>
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP (GroupVecteur3){ };
TEST (GroupVecteur3, test_vecteur_norme) { //premier test unitaire
    Vecteur3 v3 = {1, 2, 3};
    float resultat = sqrt(pow(v3.x,2)+pow(v3.y,2)+pow(v3.z,2));
    CHECK_EQUAL(resultat, Calculnorme(v3));
}

TEST (GroupVecteur3, test_vecteur_produit) { //deuxieme test unitaire
    Vecteur3 v3 = {1, 2, 3};
    Vecteur3 v4 = {1, 2, 3};
    float Prod = v3.x*v4.x + v3.y*v4.y + v3.z*v4.z;
    CHECK_EQUAL(Prod, produitScalaire(v3, v4));
}

TEST (GroupVecteur3, test_vecteur_addition) { //troisieme test unitaire
    Vecteur3 v3 = {1, 2, 3};
    Vecteur3 v4 = {1, 2, 3};
    float Som = v3.x+v4.x + v3.y+v4.y + v3.z+v4.z;
    CHECK_EQUAL(Som, addition(v3, v4));
}
