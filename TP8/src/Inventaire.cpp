#include "Inventaire.hpp"

std::ostream & operator << (std::ostream& os, Inventaire & Inv) {
	std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
	for (int i = 0; i < Inv._bouteilles.size(); i++){
		os << Inv._bouteilles[i];
	}
	std::locale::global(vieuxLoc);
	return os;
}
