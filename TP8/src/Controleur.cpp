#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
	chargerInventaire("Test");
    for (auto & v : _vues)
      v->actualiser();
}

std::string Controleur::getTexte(){
	std::ostringstream oss;
	oss << _inventaire;
	std::string str = oss.str();
	return str;
}

void Controleur::chargerInventaire(std::string a){
	std::ifstream fichier(a, std::ios::in);
	if(fichier){
		Bouteille bout;
		while(fichier >> bout){
			_inventaire._bouteilles.push_back(bout);
		}
		fichier.close();
	}
	else {
		std::cerr << "Impossible d'ouvrir le fichier !" << std::endl;
	}

	//_inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
	for (auto & v : _vues)
      v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}
