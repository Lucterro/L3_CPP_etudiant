#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <list>
#include <sort>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::list<Bouteille> _bouteilles;
    void trier();
};

std::ostream & operator << (std::ostream & os, Inventaire & Inv);
std::istream & operator >> (std::istream & is, Inventaire & Inv);


#endif
