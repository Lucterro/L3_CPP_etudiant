#include "Produit.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };
TEST(GroupProduit, test_produit_1) { // premier test unitaire
	Produit prod (1, "Poire à lavement");
	CHECK_EQUAL(prod.getId(), 1);
	CHECK_EQUAL(prod.getDescription(), "Poire à lavement");
}
