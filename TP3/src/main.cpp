#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"
#include <iostream>

int main() {
	Location loc;
	loc._idClient = 18;
	loc._idProduit = 42;
	loc.afficherLocation();
	
	Client cli (1, "Lucas");
	cli.afficherClient();
	
	Produit prod (1, "Poire à lavement");
	prod.afficherProduit();
	
	Magasin mag;
	mag.ajouterClient("Valentin");
	mag.ajouterClient("Clément");
	mag.ajouterClient("Thibault");
	mag.afficherClients();
	std::cout << mag.nbClients() << std::endl;
	mag.supprimerClient(1);
	mag.afficherClients();
	std::cout << mag.nbClients() << std::endl;
	
	mag.ajouterProduit("Poire");
	mag.ajouterProduit("Pomme");
	mag.ajouterProduit("Pêche");
	mag.afficherProduits();
	std::cout << mag.nbProduits() << std::endl;
	mag.supprimerProduit(1);
	mag.afficherProduits();
	std::cout << mag.nbProduits() << std::endl;
	
	mag.supprimerClient(8);
	mag.supprimerProduit(9);
	
    return 0;
}

