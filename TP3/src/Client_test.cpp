#include "Client.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };
TEST(GroupClient, test_client_1) { // premier test unitaire
	Client cli (1, "Lucas");
	CHECK_EQUAL(cli.getId(), 1);
	CHECK_EQUAL(cli.getNom(), "Lucas");
}
