#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"

#include <iostream>
#include <vector>
#include <string>

Magasin::Magasin () : 
	_idCourantClient (0), _idCourantProduit (0)
{}

//////////////////////////*Clients*/////////////////////////////////////

int Magasin::nbClients() const {
	return _clients.size();
}

void Magasin::ajouterClient(const std::string & nom){
	_clients.push_back(Client(_idCourantClient, nom));
	_idCourantClient ++;
}

void Magasin::afficherClients() const {
	std::cout << "\nClients : " << std::endl;
	for (int i = 0; i < _clients.size() ; i++){
		std::cout << " -> ";
		_clients.at(i).afficherClient();
	}
}

void Magasin::supprimerClient(int idClient){
	try {
		for (int i = 0; i < _clients.size() ; i++){
			if(_clients.at(i).getId() == idClient){
				std::swap(_clients.at(i), _clients.back());
				_clients.pop_back();
			}
		}
	}
	catch (const std::exception& e) {
	std::cout << "Erreur, le client" << idClient << "n'existe pas" << std::endl;
	}
}
	
//////////////////////////*Produits*/////////////////////////////////////

int Magasin::nbProduits() const {
	return _produits.size();
}

void Magasin::ajouterProduit(const std::string & nom){
	_produits.push_back(Produit(_idCourantProduit, nom));
	_idCourantProduit ++;
}

void Magasin::afficherProduits() const {
	std::cout << "\nProduits : " << std::endl;
	for (int i = 0; i < _produits.size() ; i++){
		std::cout << " -> ";
		_produits.at(i).afficherProduit();
	}
}

void Magasin::supprimerProduit(int idProduit){
	try {
		for (int i = 0; i < _produits.size() ; i++){
			if(_produits.at(i).getId() == idProduit){
				std::swap(_produits.at(i), _produits.back());
				_produits.pop_back();
			}
		}
	}
	catch (const std::exception& e) {
	std::cout << "Erreur, le client" << idProduit << "n'existe pas" << std::endl;
	}	
}
