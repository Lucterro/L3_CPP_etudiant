#ifndef LISTE_H
#define LISTE_H

struct Liste {
    float x, y, z;
    void afficher();
};

struct Noeud {
    Noeud _tete;
    int _valeur;
    Noeud _suivant;
};

Liste();

const int getTaille();

const int getElement(int indice);

void ajouterDevant(int valeur);

#endif //LISTE_H
