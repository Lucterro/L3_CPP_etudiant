#include "Doubler.hpp"

#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;

    int a = 42;
    int *p;
    p = &a;
    *p = 37;

    int * t1 = new int [10];
    t1[3] = 42;
    delete [] t1;
    t1 = nullptr;

    return 0;
}

